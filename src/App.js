import './App.css';
import { UsingArrowFunction } from './component/using-arrow-function';
import { UsingFunction } from './component/using-function';
import { UsingVariableAssignment } from './component/using-variable-assignment-function';

function App() {
  return (
    <div className="App">
       <UsingFunction />
       <UsingArrowFunction />
       <UsingVariableAssignment />
    </div>
  );
}

export default App;

// default export is early loading
//to import default export no need to enclose inside {}.
