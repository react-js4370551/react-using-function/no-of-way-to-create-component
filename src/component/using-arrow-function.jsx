//export is lazy loading
//to import this we need to enclose inside {} Ex: import {UsingArrowFunction} From './using-arrow-function'
export const UsingArrowFunction = ()=>{
    return (
        <div>
            <h1>Component using arrow function.</h1>
        </div>
    )
}